#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
# Created at  : 2021-05-06
# Last update : 2021-05-06
# Description : Description: Procura oportunidades de imóveis em Jundaí
#
# @author     : Joao Roberto Perin | jrperin@gmail.com
# ----------------------------------------------------------------------------

import requests, time, csv, os, pickle
import json


def get_houses(size=350, from_=0):
    area = 120
    quartos = 3
    garagens = 1
    limit = 0

    q = f"https://glue-api.vivareal.com/v2/listings?addressCity=Jundiaí&addressLocationId=BR>Sao Paulo>NULL>Jundiai&addressNeighborhood=&addressState=São Paulo&addressCountry=Brasil&addressStreet=&addressZone=&addressPointLat=-23.185708&addressPointLon=-46.897806&usableAreasMin={area}&bedrooms={quartos}&business=SALE&facets=amenities&unitTypes=HOME&unitSubTypes=CONDOMINIUM&unitTypesV3=CONDOMINIUM&usageTypes=RESIDENTIAL&parkingSpaces={garagens}&listingType=USED&parentId=null&categoryPage=RESULT&includeFields=search(result(listings(listing(displayAddressType,amenities,usableAreas,constructionStatus,listingType,description,title,unitTypes,nonActivationReason,propertyType,unitSubTypes,id,portal,parkingSpaces,address,suites,publicationType,externalId,bathrooms,usageTypes,totalAreas,advertiserId,bedrooms,pricingInfos,showPrice,status,advertiserContact,videoTourLink,whatsappNumber,stamps),account(id,name,logoUrl,licenseNumber,showAddress,legacyVivarealId,phones),medias,accountLink,link)),totalCount),page,seasonalCampaigns,fullUriFragments,nearby(search(result(listings(listing(displayAddressType,amenities,usableAreas,constructionStatus,listingType,description,title,unitTypes,nonActivationReason,propertyType,unitSubTypes,id,portal,parkingSpaces,address,suites,publicationType,externalId,bathrooms,usageTypes,totalAreas,advertiserId,bedrooms,pricingInfos,showPrice,status,advertiserContact,videoTourLink,whatsappNumber,stamps),account(id,name,logoUrl,licenseNumber,showAddress,legacyVivarealId,phones),medias,accountLink,link)),totalCount)),expansion(search(result(listings(listing(displayAddressType,amenities,usableAreas,constructionStatus,listingType,description,title,unitTypes,nonActivationReason,propertyType,unitSubTypes,id,portal,parkingSpaces,address,suites,publicationType,externalId,bathrooms,usageTypes,totalAreas,advertiserId,bedrooms,pricingInfos,showPrice,status,advertiserContact,videoTourLink,whatsappNumber,stamps),account(id,name,logoUrl,licenseNumber,showAddress,legacyVivarealId,phones),medias,accountLink,link)),totalCount)),account(id,name,logoUrl,licenseNumber,showAddress,legacyVivarealId,phones,phones)&size={size}&from={from_}&sort=pricingInfos.price ASC sortFilter:pricingInfos.businessType='SALE'&q=&developmentsSize=5&__vt=&levels=CITY,UNIT_TYPE&ref=/venda/sp/jundiai/condominio_residencial/&pointRadius="

    headers = {
        "x-domain": "www.vivareal.com.br",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0",
        "Accept": "*/*",
        "Accept-Encoding": "deflate",        
        "Connection": "keep-alive"
    }

    # "Accept-Encoding": "gzip, deflate, br",

    # print("HEADERS" + 10*"-")
    # print(headers)
    # print("QUERY" + 10*"-")
    # print(q)
    # print(20*"-")
    
    session = requests.session()
    response = session.get(q, headers=headers)

    j = json.loads(response.content.decode())

    itens = j.get('search', {}).get('result', {}).get('listings', [])
    pagination = j.get('page', {}).get('uriPagination', {})
    page = pagination.get('page', 0)
    total = pagination.get('total', 0)

    anuncios = list()
    cont = 0

    for iten in itens:
        root = iten.get('listing', {})
        root['href'] = iten.get('link',{}).get('href', '')
        root['title'] = iten.get('link',{}).get('name', '')
        root['medias'] = iten.get('medias',[])
        # print(f'-[{cont}]--------------------------------------------------')
        # print(root.get('description'))
        # print(type(root))
        cont += 1
        anuncios.append(root)

    return anuncios, page, total


def load_zero_values_control(filename):
    zeroValues = dict()
    if os.path.isfile(filename):
        zeroValues = json.load(open(filename))
    return zeroValues


def load_names(filename):
    names = dict()
    if os.path.isfile(filename):
        with open(filename) as cfile:
            csvReader = csv.reader(cfile)
            x = 0
            for line in csvReader:
                names[line[0]] = line[1]

    return names


def get_avg(volumes={}):
    acum = 0
    qtd = 0
    avg = 0
    for date in volumes:
        acum += int(volumes[date])
        qtd += 1

    if qtd > 0:
        avg = acum / qtd

    return avg


if __name__ == "__main__":
    print("Iniciado....")

    total = 99999999
    page = 0
    from_ = 0
    size = 350  # MAX = 350
    anuncios = list()

    while from_ < total:
        anunciosQuery, page, total = get_houses(size, from_)
        # print(json.dumps(anunciosQuery))
        anuncios = anuncios + anunciosQuery
        print(f'Processando Pagina {page}, de {from_} tamanho: {size} - Total: {total}')
        time.sleep(5)

        # Prepara para proxima iteracao
        from_ += size

    # Salva resultado
    with open("consulta_casas.txt", "wb") as fp:  # Pickling
        pickle.dump(anuncios, fp)

    print("Encerrado....")
