# Analise de Casas em Jundiaí - Site Viva Real

**Referencia:** <https://www.digitalocean.com/community/tutorials/how-to-make-a-web-application-using-flask-in-python-3-pt>


## PREPARAR

> **Passos:**
> 1. python -m venv venv
> 1. source venv/bin/activate
> 1. pip install flask
> 1. python -c "import flask; print(flask.__version__)"
> 1. export FLASK_APP=app
> 1. export FLASK_ENV=development
> 1. flask run
> 1. Alternativa para executar (passos 5, 6 e 7):  
> `FLASK_APP=app FLASK_ENV=development flask run`

> **docker-compose - MySql:**
> 1. `docker-compose -f docker-compose_mysql.yml up -d`
> 1. Criar tabela do banco de dados: `./00_create_database.sh`

## EXECUTAR
> **Passos:**
> 1. `docker-compose -f docker-compose_mysql.yml up -d`
> 1. `source venv/bin/activate`
> 1. `01_consulta.py` _<-- Consulta no site da Viva Real_
> 1. `02_load_to_mysql.py` _<-- Insere consulta no mysql_
> 1. `cd site`
> 1. `run.sh` _<-- Executa o site_
