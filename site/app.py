from flask import Flask, render_template, request, url_for, flash, redirect
from werkzeug.exceptions import abort
from babel.numbers import format_currency, format_decimal
import mysql.connector

app = Flask(__name__)
app.config['SECRET_KEY'] = '+cteRnm9L&$v2!6=ps3DFEVh'

def get_conn():
    config = {
        "user": "root",
        "password": "root",
        "host": "127.0.0.1",
        "port": 3306,
        "database": "analise_imoveis_jundiai"
    }
    try:
        cn = mysql.connector.connect(**config)
        cur = cn.cursor(dictionary=True)
        return cn, cur
    except:
        print ("Mysql connection error")
        exit(1)


# --- HOME ---------------------------------------------

@app.route('/')
def home():
    return render_template('index.html',)


# --- CASAS VIVA REAL ----------------------------------

@app.route('/imoveis_viva_real/')
def imoveis_viva_real():

    args = request.args
    print(args)

    cn, cur = get_conn()
    price_max = int("0" + args.get("price_max","0"))
    if  price_max > 0:
        cur.execute(f'SELECT * FROM imoveis_viva_real WHERE price <= {price_max} AND discarded = FALSE AND businessType = "SALE" AND neighborhood NOT IN (SELECT name FROM bairros WHERE discarded = TRUE) ORDER BY price ASC LIMIT 100')
    else:
        cur.execute('SELECT * FROM imoveis_viva_real WHERE discarded = FALSE AND businessType = "SALE" AND neighborhood NOT IN (SELECT name FROM bairros WHERE discarded = TRUE) ORDER BY price ASC LIMIT 100')

    imoveis_viva_real = cur.fetchall()
    imoveis_viva_real = map(fix_imovel, imoveis_viva_real)

    return render_template('imoveis_viva_real.html', casas=imoveis_viva_real)

@app.route('/imoveis_viva_real/<int:id>/edit/', methods=['POST'])
def imoveis_viva_real_edit(id):
    print("-" * 30)
    print(request.form)
    print("-" * 30)
    starred = bool(request.form.get('starred', False))
    discarded = bool(request.form.get('discarded', False))
    checked = bool(request.form.get('checked', False))

    print("Variaveis recebidas")
    print(id, starred, discarded, checked)
    print("-" * 30)

    cn, cur = get_conn()
    x = cur.execute('UPDATE imoveis_viva_real SET starred = %s, discarded = %s, checked = %s'
                    ' WHERE id = %s',
                    (starred, discarded, checked, id))

    print("Retorno UPDATE db")
    print(x)
    print("-" * 50)

    cn.commit()
    cn.close()
    return redirect(url_for('imoveis_viva_real'))

def fix_imovel(imovel):
    imovel['href'] = f'https://www.vivareal.com.br{imovel.get("href","")}'
    imovel['description'] = imovel.get("description","").replace("<br><br>","<br>").replace(",",", ").replace(" ,",", ").replace("*","\U0001F535 ").replace("  ", " ")
    return imovel

# --- BAIRROS ------------------------------------------

@app.route('/bairros/')
def bairros():
    cn, cur = get_conn()
    cur.execute('SELECT * FROM bairros')
    bairros = cur.fetchall()
    return render_template('bairros.html', bairros=bairros)

@app.route('/bairros/<string:name>/edit/', methods=['POST'])
def bairro_edit(name):

    print("-"*30)
    print (request.form)
    print("-"*30)
    starred  = bool(request.form.get('starred', False))
    discarded = bool(request.form.get('discarded', False))
    checked   = bool(request.form.get('checked', False))

    print("Variaveis recebidas")
    print(name, starred, discarded, checked)
    print("-"*30)

    cn, cur = get_conn()
    x = cur.execute('UPDATE bairros SET starred = %s, discarded = %s, checked = %s'
                    ' WHERE name = %s',
                    (starred, discarded, checked, name))
    
    print("Retorno UPDATE db")
    print(x)
    print("-"*50)

    cn.commit()
    cn.close()
    return redirect(url_for('bairros'))





# --- FILTROS ------------------------------------------

@app.template_filter()
def br_currency(value):
   return format_currency(value, 'BRL', locale='pt_BR')

app.jinja_env.filters['br_currency'] = br_currency
'''
    Modo de usar no Jinja:
    {{ '-10000.500' | br_real }}
'''

@app.template_filter()
def br_float(value):
    return format_decimal(value,  format='#,##0.##;-#', locale='pt_BR')

app.jinja_env.filters['br_float'] = br_float
'''
    Modo de usar no Jinja:
    {{ '-10000.500' | br_float }}
'''    




# --- NAO USADO... -------------------------------------

@app.route('/<int:post_id>')
def post(post_id):
    post = get_post(post_id)
    return render_template('post.html', post=post)

@app.route('/create', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        title = request.form['title']
        content = request.form['content']

        if not title:
            flash('Title is required!')
        else:
            conn = get_db_connection()
            conn.execute('INSERT INTO posts (title, content) VALUES (?, ?)',
                         (title, content))
            conn.commit()
            conn.close()
            return redirect(url_for('index'))

    return render_template('create.html')


@app.route('/<string:name>/edit', methods=('GET', 'POST'))
def edit(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        content = request.form['content']

        if not title:
            flash('Title is required!')
        else:
            conn = get_db_connection()
            conn.execute('UPDATE posts SET title = ?, content = ?'
                         ' WHERE id = ?',
                         (title, content, id))
            conn.commit()
            conn.close()
            return redirect(url_for('index'))

    return render_template('edit.html', post=post)


@app.route('/<int:id>/delete', methods=('POST',))
def delete(id):
    post = get_post(id)
    conn = get_db_connection()
    conn.execute('DELETE FROM posts WHERE id = ?', (id,))
    conn.commit()
    conn.close()
    flash('"{}" was successfully deleted!'.format(post['title']))
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run('0.0.0.0', 5000, debug=True)