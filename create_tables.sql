CREATE DATABASE IF NOT EXISTS analise_imoveis_jundiai;
USE analise_imoveis_jundiai;

DROP TABLE IF EXISTS imoveis_viva_real;
CREATE TABLE imoveis_viva_real (
id VARCHAR(50) NOT NULL PRIMARY KEY,
externalId VARCHAR(50),
advertiserId VARCHAR(50),
description TEXT,
usableAreas FLOAT,
totalAreas FLOAT,
portal VARCHAR(100),
parkingSpaces TINYINT(1),
suites TINYINT(1),
bathrooms TINYINT(1),
bedrooms TINYINT(1),
businessType VARCHAR(50),
yearlyIptu FLOAT,
price FLOAT,
price_per_usableAreas FLOAT,
monthlyCondoFee FLOAT,
status VARCHAR(50),
whatsappNumber VARCHAR(50),
amenities TEXT,
country VARCHAR (50),
zipcode VARCHAR (30),
city VARCHAR(100),
stateAcronym VARCHAR(2),
street VARCHAR(255),
neighborhood VARCHAR(120),
complement VARCHAR(255),
href TEXT,
title TEXT,
image1 TEXT,
image2 TEXT,
image3 TEXT,
hasAirConditioner BOOLEAN,
hasBarbecueGrill BOOLEAN,
hasPool BOOLEAN,
neighborhood_discarded BOOLEAN,
starred BOOLEAN,
discarded BOOLEAN,
checked BOOLEAN,
updateDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS bairros;
CREATE TABLE bairros (
name VARCHAR(120) NOT NULL PRIMARY KEY,
discarded BOOLEAN,
starred BOOLEAN,
checked BOOLEAN,
updateDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);
