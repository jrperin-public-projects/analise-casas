#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------
# Created at  : 2021-05-11
# Last update : 2021-05-11
# Description : Description: Pega arquivo gerado do processo de consulta e
#               carrega no mysql
#
# @author     : Joao Roberto Perin | jrperin@gmail.com
# ----------------------------------------------------------------------------

import pickle
import mysql.connector
import json
import re


mydb = None


def remove_emoji(value):
    emoji_pattern = re.compile("["
                        u"\U0001F600-\U0001F64F"  # emoticons
                        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                        u"\U0001F680-\U0001F6FF"  # transport & map symbols
                        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                        u"\U00002702-\U000027B0"
                        u"\U000024C2-\U0001F251"
                        "]+", flags=re.UNICODE)

    return emoji_pattern.sub('', value)

def get_db_conn():
    global mydb
    if not mydb:
        # Mysql do projeto site_analise_digital:
        mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="root",
            database="analise_imoveis_jundiai"
        )
    return mydb


def processa():
    # Restaura resultado
    with open("consulta_casas.txt", "rb") as fp:  # Unpickling
        anuncios = pickle.load(fp)

    cursor = get_db_conn().cursor()

    bairros = dict()
    count = 0
    duplicados = 0
    textoinvalido = 0
    check_duplicates = dict()

    # INSERE ANUNCIOS
    for anuncio in anuncios:

        #print(json.dumps(anuncio, indent=4))

        id1 = anuncio.get('id', '')
        externalId = anuncio.get('externalId', '')
        advertiserId = anuncio.get('advertiserId', '')
        description = remove_emoji(anuncio.get('description', ''))
        bytes(description, 'utf-8').decode('utf-8', 'ignore')

        usableAreas = float(anuncio.get('usableAreas', [])[0] if len(anuncio.get('usableAreas', [])) > 0 else '-1.0')
        totalAreas = float(anuncio.get('totalAreas', [])[0] if len(anuncio.get('totalAreas', [])) > 0 else '-1.0')
        portal = anuncio.get('portal', '')
        parkingSpaces = anuncio.get('parkingSpaces', [])[0] if len(anuncio.get('parkingSpaces', [])) > 0 else -1
        suites = anuncio.get('suites', [])[0] if len(anuncio.get('suites', [])) > 0 else -1
        bathrooms = anuncio.get('bathrooms', [])[0] if len(anuncio.get('bathrooms', [])) > 0 else -1
        bedrooms = anuncio.get('bedrooms', [])[0] if len(anuncio.get('bedrooms', [])) > 0 else -1

        pricingInfos = anuncio.get('pricingInfos', [])
        # ----------------------------------------------------------------------------------------------------
        businessType = pricingInfos[0].get('businessType', '') if len(pricingInfos) > 0 else ''
        yearlyIptu = float(pricingInfos[0].get('yearlyIptu', '-1.0') if len(pricingInfos) > 0 else '-1.0')
        price = float(pricingInfos[0].get('price', '-1.0') if len(pricingInfos) > 0 else '-1.0')
        price_per_usableAreas = price / usableAreas
        monthlyCondoFee = float(pricingInfos[0].get('monthlyCondoFee', '-1.0') if len(pricingInfos) > 0 else '-1.0')
        # ----------------------------------------------------------------------------------------------------

        status = anuncio.get('status', '')
        whatsappNumber = anuncio.get('whatsappNumber', '')
        amenities = ', '.join(anuncio.get('amenities', []))
        country = anuncio.get('address', {}).get('country', '')
        zipcode = anuncio.get('address', {}).get('zipcode', '')
        city = anuncio.get('address', {}).get('city', '')
        stateAcronym = anuncio.get('address', {}).get('stateAcronym', '')
        street = anuncio.get('address', {}).get('street', '')
        neighborhood = anuncio.get('address', {}).get('neighborhood', f'NaoEncontrado_{id1}')
        complement = anuncio.get('address', {}).get('complement', '')
        href = anuncio.get('href', '')
        title = anuncio.get('title', '')

        medias = [None]*3
        cont = 0
        for media in anuncio.get('medias',[]):
            if media.get('type','') == 'IMAGE':
                medias[cont] = media.get('url','').replace('{action}', 'fit-in').replace('{width}x{height}', '360x480')
                cont +=1
            if cont > 2:
                break

        hasAirConditioner = False
        hasBarbecueGrill = False
        hasPool = False
        starred = False
        discarded = False

        bairros[neighborhood] = False

        sql = "INSERT INTO imoveis_viva_real (" \
              "id, externalId, advertiserId, description, usableAreas, totalAreas, portal, parkingSpaces, " \
              "suites, bathrooms, bedrooms, businessType, yearlyIptu, price, price_per_usableAreas, monthlyCondoFee, status, " \
              "whatsappNumber, amenities, country, zipcode, city, stateAcronym, street, neighborhood, " \
              "complement, title, href, image1, image2, image3, hasAirConditioner, hasBarbecueGrill, " \
              "hasPool, starred, discarded " \
              ") VALUES (" \
              "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s," \
              "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s," \
              "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s," \
              "%s, %s, %s, %s, %s, %s" \
              ")"
        val = (id1, externalId, advertiserId, description, usableAreas, totalAreas, portal, parkingSpaces,
               suites, bathrooms, bedrooms, businessType, yearlyIptu, price, price_per_usableAreas, monthlyCondoFee, status,
               whatsappNumber, amenities, country, zipcode, city, stateAcronym, street, neighborhood,
               complement, title, href, medias[0], medias[1], medias[2], hasAirConditioner, hasBarbecueGrill, hasPool, starred, discarded)

        # print(f'[{count} | {id1}, {externalId}, {advertiserId}]-------------------------'
        #      f'------------------------------------------------------------------------'[0:80])

        try:
            cursor.execute(sql, val)
        except mysql.connector.Error as err:
            if err.errno == 1062:  # Duplicate key
                print(30 * '-')
                print(val)
                print(err.errno)
                print(err.msg)
                if check_duplicates.get(id1):
                    print(f'Duplicado com item: {check_duplicates.get(id1)}')
                print(30 * '-')
                duplicados += 1
            elif err.errno == 1366:  # Incorrect String Value
                print(30 * '-')
                print('Conteúdo inválido no registro:')
                print(description)
                print(err.errno)
                print(err.msg)
                print(30 * '-')
                textoinvalido += 1
            else:
                raise err

        check_duplicates[id1] = count

        get_db_conn().commit()

        count += 1

    print(30 * '-')
    print(f'Total      = {count}')
    print(f'Duplicados = {duplicados}')
    print(f'Texto Inv. = {textoinvalido}')
    print(30 * '-')

    # INSERE BAIRROS
    count = 0
    for bairro in bairros:
        if bairro.strip() != "":
            sql = "INSERT INTO bairros ( name, discarded, starred ) " \
                  "VALUES ( %s, %s, %s )"
            val = (bairro, False, False)

            try:
                cursor.execute(sql, val)
            except mysql.connector.errors.IntegrityError as err:
                if err.errno == 1062:  # Duplicate key
                    print(30 * '-')
                    print(val)
                    print(30 * '-')
                    print(err.errno)
                    print(err.msg)
                    print(30 * '-')
                    duplicados += 1
                else:
                    raise err

            get_db_conn().commit()

            print(f'{count} - {bairro}')
            count += 1

        else:
            print("Bairro com valor zerado...")

    print(30 * '-')
    print(f'Total = {count}')
    print(30 * '-')


if __name__ == "__main__":
    print("Iniciado....")

    processa()

    print()
    print("Encerrado....")
